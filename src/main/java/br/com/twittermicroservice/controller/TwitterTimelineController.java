package br.com.twittermicroservice.controller;

import br.com.twittermicroservice.dto.ErrorStatusDTO;
import br.com.twittermicroservice.service.TwitterAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api")
public class TwitterTimelineController {

    @Autowired
    private TwitterAPIService service;

    /**
     * Retorna a timeline de um usuário do Twitter
     *
     * @param userName nome do usuário
     * @param pageSize page size (Opicional) máximo 200
     * @return Lista de Tweets
     */
    @ResponseBody
    @RequestMapping(value = "/timeline/{userName}", method = RequestMethod.GET)
    public List<Tweet> getPublicTimeline(@PathVariable String userName, Integer pageSize){
        return service.getPublicTimeline(userName, pageSize);
    }

    /**
     *
     * @param e Exceção lançada
     * @return Retorna um DTO com apenas uma String que será convertida para JSON
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody public ErrorStatusDTO exceptionHandler(Exception e){
        return new ErrorStatusDTO(e.getMessage());
    }

}
