package br.com.twittermicroservice.controller;

import br.com.twittermicroservice.service.TwitterAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class IndexController {

    @Autowired
    private TwitterAPIService twitterAPIService;

    @Autowired
    private ConnectionRepository connectionRepository;

    /**
     * Apenas da um redirect para a view de conexão com Twitter
     *
     * @return Url de redirect
     */
    @RequestMapping(method=RequestMethod.GET)
	public String index() {
        return "redirect:/connect/Twitter";
	}

}
