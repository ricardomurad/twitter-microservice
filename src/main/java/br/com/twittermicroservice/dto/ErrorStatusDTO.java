package br.com.twittermicroservice.dto;


public class ErrorStatusDTO {

    private String message;

    public ErrorStatusDTO(String message){
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
