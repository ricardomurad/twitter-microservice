package br.com.twittermicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    /**
     *  CLasse do springBoot que inicia o tomcat, contexto e demais
     *
     * @param args main args
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
