package br.com.twittermicroservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class TwitterAPIService {

    public static final int DEFAULT_PAGE_SIZE = 20;
    @Autowired
    private ConnectionRepository connectionRepository;


    @Autowired
    Twitter twitter;

    public List<Tweet> getUserTimeline(String username){

        if(username == null || username.isEmpty()){
            throw new IllegalArgumentException("Usuario Inválido");
        }

        List<Connection<Twitter>> connections = connectionRepository.findConnections(Twitter.class);

        connections.stream().filter(c -> c.getDisplayName().equals("@" + username));

        return twitter.timelineOperations().getUserTimeline();
    }


    public List<Tweet> getPublicTimeline(String username, Integer pageSize){

        if(username == null || username.isEmpty()){
            throw new IllegalArgumentException("Usuario Inválido");
        }

        if(pageSize == null){
            pageSize = DEFAULT_PAGE_SIZE;
        }

        if(pageSize > 200){
            throw  new IllegalArgumentException("Atributo pageSize não pode ser maior que 200");
        }

        return twitter.timelineOperations().getUserTimeline(username, pageSize);
    }

    public List<Connection<Twitter>> listAuthorizedUsers() {
        return connectionRepository.findConnections(Twitter.class);
    }

}
